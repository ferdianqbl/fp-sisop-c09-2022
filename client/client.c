#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#define LOCALPORT 9999
#define BUFFER_LEN 256

#define ERROR_SOCKET_CREATE -1
#define ERROR_SOCKET_CONNECT -2
#define ERROR_SOCKET_BIND -3
#define ERROR_SOCKET_LISTEN -4

#define ROOT_UID 0

#define EXIT_COMMAND "EXIT"

void error(int errcode)
{
    switch (errcode)
    {
    case ERROR_SOCKET_CREATE:
        printf("FAILED TO CREATE SOCKET\n");
        break;
    case ERROR_SOCKET_CONNECT:
        printf("FAILED TO CONNECT SOCKET\n");
    default:
        break;
    }

    exit(errcode);
}

int main(int argc, char *argv[])
{
    // CREATE SOCKET
    int client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket < 0)
        error(ERROR_SOCKET_CREATE);

    // SET SOCKET ADDRESS UP
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(LOCALPORT);
    server_address.sin_addr.s_addr = INADDR_ANY;

    int connection_status = connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address));
    if (connection_status < 0)
        error(ERROR_SOCKET_CONNECT);

    char client_msg[BUFFER_LEN];
    char server_msg[BUFFER_LEN];

    int uid = getuid();
    if (uid == ROOT_UID)
    {
        printf("LOGGED IN AS ROOT");
        strcpy(client_msg, "ROOT");
        send(client_socket, client_msg, BUFFER_LEN, 0);
    }
    else
    {
        printf("LOGGED IN AS USER\n");
        char user_active[BUFFER_LEN];
        strcpy(user_active, argv[2]);
        strcat(user_active, "\t");
        strcat(user_active, argv[4]);
        strcat(user_active, "\n");
        send(client_socket, user_active, strlen(user_active), 0);
    }

    bzero(server_msg, BUFFER_LEN);
    recv(client_socket, server_msg, BUFFER_LEN, 0);
    if (!strcmp(server_msg, "USER NOT FOUND"))
    {
        printf("%s\n", server_msg);
        close(client_socket);
        exit(0);
    }

    printf("\n\n%s\n\n", server_msg);

    while (1)
    {
        bzero(client_msg, BUFFER_LEN);
        bzero(server_msg, BUFFER_LEN);

        fgets(client_msg, BUFFER_LEN, stdin);
        client_msg[strlen(client_msg) - 1] = 0;

        send(client_socket, client_msg, strlen(client_msg), 0);

        if (!strcmp(client_msg, EXIT_COMMAND))
            break;
        recv(client_socket, server_msg, BUFFER_LEN, 0);
        printf("%s\n", server_msg);
    }

    close(client_socket);
    return 0;
}