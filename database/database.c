#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>

#define LOCALPORT 9999
#define STRING_LENGTH 2048
#define PATH_LENGTH STRING_LENGTH * 2

#define ERROR_SOCKET_CREATE -1
#define ERROR_SOCKET_CONNECT -2
#define ERROR_SOCKET_BIND -3
#define ERROR_SOCKET_LISTEN -4

#define ROOT_UID 0
#define USER_UID 1

#define EXIT_COMMAND "EXIT"

// status_handler
#define SUCCESS_CREATE 0
#define SUCCESS_ACCESS 1
#define SUCCESS_DROP 2
#define SUCCESS_GRANT 3
#define SUCCESS_INSERT 4
#define ROOT_MODE -1
#define CANNOT_OPEN -2
#define CANNOT_DROP -3
#define DATABASE_OR_TABLE_EXIST -4
#define DATABASE_OR_TABLE_NOT_EXIST -5
#define PERMISSION_DENIED -6
#define USER_NOT_EXIST -7
#define NO_DATABASE_SELECTED -8
#define INPUT_VALUE_NOT_MATCH_COLUMN_TOTAL -9
#define DATATYPE_NOT_MATCH -10
#define FILE_NOT_FOUND -11
#define WRONG_COMMAND -99

char status_msg[STRING_LENGTH];
char username_active[STRING_LENGTH];
char globalPath[STRING_LENGTH];
char userPermission[STRING_LENGTH];
char tempCommand[STRING_LENGTH];

void check(int call, int errcode)
{
    if (call < 0)
    {
        exit(errcode);
    }
}
void status_handler(int errCode)
{
    bzero(status_msg, STRING_LENGTH);
    switch (errCode)
    {
    case SUCCESS_CREATE:
        strcpy(status_msg, "SUCCESS CREATE\n");
        break;
    case SUCCESS_ACCESS:
        strcpy(status_msg, "SUCCESS ACCESS\n");
        break;
    case SUCCESS_DROP:
        strcpy(status_msg, "SUCCESS DROP\n");
        break;
    case SUCCESS_GRANT:
        strcpy(status_msg, "SUCCESS GRANT\n");
        break;
    case SUCCESS_INSERT:
        strcpy(status_msg, "SUCCESS INSERT\n");
        break;
    case ROOT_MODE:
        strcpy(status_msg, "YOU SHOULD IN ROOT MODE\n");
        break;
    case CANNOT_OPEN:
        strcpy(status_msg, "CANNOT BE OPEN\n");
        break;
    case CANNOT_DROP:
        strcpy(status_msg, "CANNOT BE DROP\n");
        break;
    case DATABASE_OR_TABLE_EXIST:
        strcpy(status_msg, "YOUR DATABASE OR TABLE EXIST\n");
        break;
    case DATABASE_OR_TABLE_NOT_EXIST:
        strcpy(status_msg, "YOUR DATABASE OR TABLE NOT EXIST\n");
        break;
    case PERMISSION_DENIED:
        strcpy(status_msg, "PERMISSION DENIED\n");
        break;
    case USER_NOT_EXIST:
        strcpy(status_msg, "USER NOT EXIST\n");
        break;
    case NO_DATABASE_SELECTED:
        strcpy(status_msg, "NO DATABASE SELECTED\n");
        break;
    case INPUT_VALUE_NOT_MATCH_COLUMN_TOTAL:
        strcpy(status_msg, "INPUT VALUE NOT MATCH WITH COLUMN TOTAL\n");
        break;
    case DATATYPE_NOT_MATCH:
        strcpy(status_msg, "DATATYPE NOT MATCH\n");
        break;
    case FILE_NOT_FOUND:
        strcpy(status_msg, "FILE NOT FOUND\n");
        break;
    case WRONG_COMMAND:
        strcpy(status_msg, "YOUR COMMAND IS NOT CORRECT\n");
        break;
    default:
        break;
    }
}

void create(char client_msg[STRING_LENGTH], int uid)
{
    char cmd[STRING_LENGTH];
    strcpy(cmd, client_msg);
    char *token = strtok(cmd, " ");
    if (token && !strcmp(token, "CREATE"))
    {
        token = strtok(NULL, " ");
        if (token && !strcmp(token, "USER"))
        {
            if (uid == ROOT_UID)
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char temp_user[STRING_LENGTH];
                    strcpy(temp_user, token);
                    token = strtok(NULL, " ");
                    if (token && !strcmp(token, "IDENTIFIED"))
                    {
                        token = strtok(NULL, " ");
                        if (token && !strcmp(token, "BY"))
                        {
                            token = strtok(NULL, " ");
                            if (token)
                            {
                                char temp_pass[STRING_LENGTH];
                                strcpy(temp_pass, token);
                                FILE *user_fp = fopen("users.txt", "a");
                                fprintf(user_fp, "%s\t%s\n", temp_user, temp_pass);
                                fclose(user_fp);
                                status_handler(SUCCESS_CREATE);
                            }
                            else
                                status_handler(WRONG_COMMAND);
                        }
                        else
                            status_handler(WRONG_COMMAND);
                    }
                    else
                        status_handler(WRONG_COMMAND);
                }
                else
                    status_handler(WRONG_COMMAND);
            }
            else
                status_handler(ROOT_MODE);
        }
        else if (token && !strcmp(token, "DATABASE"))
        {
            token = strtok(NULL, " ");
            if (token)
            {
                char db_name[STRING_LENGTH];
                strcpy(db_name, token);
                DIR *dir = opendir(db_name);
                if (dir)
                {
                    status_handler(DATABASE_OR_TABLE_EXIST);
                    closedir(dir);
                }
                else if (ENOENT == errno)
                {
                    mkdir(db_name, 0777);
                    strcat(db_name, "/usersPermission.txt");
                    FILE *fp = fopen(db_name, "a");
                    if (uid == ROOT_UID)
                        fprintf(fp, "%s\n", "root");
                    else
                    {
                        char temp_user[STRING_LENGTH];
                        strcpy(temp_user, username_active);
                        fprintf(fp, "%s\n", strtok(temp_user, "\t"));
                    }
                    fclose(fp);
                    status_handler(SUCCESS_CREATE);
                }
                else
                    status_handler(CANNOT_OPEN);
            }
            else
                status_handler(WRONG_COMMAND);
        }
        else if (token && !strcmp(token, "TABLE"))
        {
            token = strtok(NULL, " ");
            if (token && strlen(globalPath) > 0)
            {
                char temp_table[strlen(token)];
                strcpy(temp_table, token);

                char table_path[PATH_LENGTH * 2];
                sprintf(table_path, "%s/%s.txt", globalPath, temp_table);
                printf("%s\n", table_path);
                FILE *tablePath = fopen(table_path, "r");
                if (tablePath)
                {
                    fclose(tablePath);
                    status_handler(DATABASE_OR_TABLE_EXIST);
                }
                char column_names[STRING_LENGTH];
                char column_data_types[STRING_LENGTH];
                bzero(column_names, STRING_LENGTH);
                bzero(column_data_types, STRING_LENGTH);
                token = strtok(NULL, " (");
                if (!token)
                {
                    status_handler(WRONG_COMMAND);
                    return;
                }
                while (token)
                {
                    char col[STRING_LENGTH];
                    strcpy(col, token);
                    strcat(col, "\t");
                    strcat(column_names, col);

                    token = strtok(NULL, " ");
                    if (token)
                    {
                        char type[STRING_LENGTH];
                        strcpy(type, token);
                        int i = strlen(type);
                        while (!(type[i] >= 'a' && type[i] <= 'z') && !(type[i] >= 'A' && type[i] <= 'Z'))
                            type[i--] = '\0';

                        if (!strcmp(type, "int") || !strcmp(type, "float") || !strcmp(type, "string"))
                        {
                            strcat(type, "\t");
                            strcat(column_data_types, type);
                        }
                        else
                            return status_handler(WRONG_COMMAND);
                    }
                    else
                        status_handler(WRONG_COMMAND);
                    token = strtok(NULL, " ");
                }

                tablePath = fopen(table_path, "a");
                fprintf(tablePath, "%s\n%s\n", column_names, column_data_types);
                fclose(tablePath);
                status_handler(SUCCESS_CREATE);
            }
            else
                status_handler(WRONG_COMMAND);
        }
        else
            status_handler(WRONG_COMMAND);
    }
    else
        status_handler(WRONG_COMMAND);
}

void use(char client_msg[STRING_LENGTH], int uid)
{
    char cmd[STRING_LENGTH];
    strcpy(cmd, client_msg);
    char *token = strtok(cmd, " ");
    if (token && !strcmp(token, "USE"))
    {
        token = strtok(NULL, " ");
        if (token)
        {
            char db_name[STRING_LENGTH];
            strcpy(db_name, token);
            strcat(db_name, "/usersPermission.txt");
            if (uid == ROOT_UID)
            {
                strcpy(globalPath, token);
                strcpy(userPermission, db_name);
                status_handler(SUCCESS_ACCESS);
                return;
            }
            FILE *users_list = fopen(db_name, "r");
            if (users_list)
            {
                char user_check[STRING_LENGTH];
                char temp_user[STRING_LENGTH];
                strcpy(temp_user, username_active);
                strcpy(status_msg, "YOU DON'T HAVE ACCESS IN THIS DATABASE\n");

                while (fgets(user_check, STRING_LENGTH, users_list) != NULL)
                {
                    char *user_token = strtok(temp_user, "\t");
                    while (user_token[strlen(user_token) - 1] == '\n' || user_token[strlen(user_token) - 1] == '\t')
                        user_token[strlen(user_token) - 1] = '\0';
                    while (user_check[strlen(user_check) - 1] == '\n' || user_check[strlen(user_check) - 1] == '\t')
                        user_check[strlen(user_check) - 1] = '\0';
                    if (strcmp(user_check, temp_user) == 0)
                    {
                        strcpy(globalPath, token);
                        strcpy(userPermission, db_name);
                        status_handler(SUCCESS_ACCESS);
                    }
                }
                fclose(users_list);
            }
            else
                status_handler(CANNOT_OPEN);
        }
        else
            status_handler(WRONG_COMMAND);
    }
    else
        status_handler(WRONG_COMMAND);
}

int checkUserPermission(char permissionPath[STRING_LENGTH], int uid)
{
    if (uid == ROOT_UID)
        return 1;
    FILE *usersPermission = fopen(permissionPath, "r");
    char user_check[STRING_LENGTH];
    char temp_user[STRING_LENGTH];
    strcpy(temp_user, username_active);
    while (fgets(user_check, STRING_LENGTH, usersPermission) != NULL)
    {
        char *user_token = strtok(temp_user, "\t");
        while (user_token[strlen(user_token) - 1] == '\n' || user_token[strlen(user_token) - 1] == '\t')
            user_token[strlen(user_token) - 1] = '\0';
        while (user_check[strlen(user_check) - 1] == '\n' || user_check[strlen(user_check) - 1] == '\t')
            user_check[strlen(user_check) - 1] = '\0';
        if (strcmp(user_check, temp_user) == 0)
            return 1;
    }
    fclose(usersPermission);
    return 0;
}

int checkUserExist(char userPath[STRING_LENGTH], char user[STRING_LENGTH])
{
    FILE *users_list = fopen(userPath, "r");
    char user_check[STRING_LENGTH];

    while (fgets(user_check, STRING_LENGTH, users_list) != NULL)
    {
        if (!strcmp(strtok(user_check, "\t"), user))
        {
            fclose(users_list);
            return 1;
        }
    }
    fclose(users_list);
    return 0;
}

void drop(char client_msg[STRING_LENGTH], int uid)
{
    char cmd[STRING_LENGTH];
    strcpy(cmd, client_msg);
    char *token = strtok(cmd, " ");
    if (token && !strcmp(token, "DROP"))
    {
        token = strtok(NULL, " ");
        if (token && !strcmp(token, "DATABASE"))
        {
            token = strtok(NULL, " ");
            if (token)
            {
                char db_name[STRING_LENGTH];
                char db_path[STRING_LENGTH];
                strcpy(db_name, token);
                strcpy(db_path, token);
                DIR *dir = opendir(db_name);
                strcat(db_path, "/usersPermission.txt");
                if (dir)
                {
                    int permission = checkUserPermission(db_path, uid);
                    if (permission)
                    {
                        struct dirent *fileExist = readdir(dir);
                        char file_remove[PATH_LENGTH * 2];
                        while (fileExist)
                        {
                            if (fileExist->d_type == DT_REG)
                            {
                                bzero(file_remove, PATH_LENGTH * 2);
                                sprintf(file_remove, "%s/%s", db_name, fileExist->d_name);
                                remove(file_remove);
                            }
                            fileExist = readdir(dir);
                        }
                        remove(db_name);
                        bzero(globalPath, STRING_LENGTH);
                        bzero(userPermission, STRING_LENGTH);
                        status_handler(SUCCESS_DROP);
                    }
                    else
                        status_handler(PERMISSION_DENIED);
                    closedir(dir);
                }
                else
                    status_handler(DATABASE_OR_TABLE_NOT_EXIST);
            }
            else
                status_handler(WRONG_COMMAND);
        }
        else if (token && !strcmp(token, "TABLE"))
        {
            if (strlen(globalPath) <= 0)
                status_handler(NO_DATABASE_SELECTED);
            else
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char temp_tb_path[PATH_LENGTH * 2];
                    char temp_tb[STRING_LENGTH];
                    strcpy(temp_tb, token);
                    sprintf(temp_tb_path, "%s/%s.txt", globalPath, temp_tb);
                    FILE *tb_exist = fopen(temp_tb_path, "r");
                    if (tb_exist)
                    {
                        fclose(tb_exist);
                        remove(temp_tb_path);
                        status_handler(SUCCESS_DROP);
                    }
                    else
                        status_handler(DATABASE_OR_TABLE_NOT_EXIST);
                }
                else
                    status_handler(WRONG_COMMAND);
            }
        }
        else if (token && !strcmp(token, "COLUMN"))
        {
        }
        else
            status_handler(WRONG_COMMAND);
    }
    else
        status_handler(WRONG_COMMAND);
}

void grant(char client_msg[STRING_LENGTH], int uid)
{
    if (uid == ROOT_UID)
    {
        char cmd[STRING_LENGTH];
        strcpy(cmd, client_msg);
        char *token = strtok(cmd, " ");
        if (token && !strcmp(token, "GRANT"))
        {
            token = strtok(NULL, " ");
            if (token && !strcmp(token, "PERMISSION"))
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char temp_db[STRING_LENGTH];
                    strcpy(temp_db, token);
                    token = strtok(NULL, " ");
                    if (token && !strcmp(token, "INTO"))
                    {
                        token = strtok(NULL, " ");
                        if (token)
                        {
                            char temp_user[STRING_LENGTH];
                            strcpy(temp_user, token);
                            int isUserExist = checkUserExist("users.txt", temp_user);
                            if (isUserExist)
                            {
                                DIR *dir = opendir(temp_db);
                                if (dir)
                                {
                                    closedir(dir);
                                    strcat(temp_db, "/usersPermission.txt");
                                    FILE *usersPermissionFile = fopen(temp_db, "a");
                                    fprintf(usersPermissionFile, "%s\n", temp_user);
                                    fclose(usersPermissionFile);
                                    status_handler(SUCCESS_GRANT);
                                }
                                else
                                    status_handler(DATABASE_OR_TABLE_NOT_EXIST);
                            }
                            else
                                status_handler(USER_NOT_EXIST);
                        }
                        else
                            status_handler(WRONG_COMMAND);
                    }
                    else
                        status_handler(WRONG_COMMAND);
                }
                else
                    status_handler(WRONG_COMMAND);
            }
            else
                status_handler(WRONG_COMMAND);
        }
        else
            status_handler(WRONG_COMMAND);
    }
    else
        status_handler(ROOT_MODE);
}

int intValidation(char *str)
{
    int i = 0;
    while (str[i] != '\0')
    {
        if (str[i] < '0' || str[i] > '9')
            return 0;
        i++;
    }
    return 1;
}

int floatValidation(char *str)
{
    int i = 0;
    int dotCount = 0;
    while (str[i] != '\0')
    {
        if (str[i] == '.')
            dotCount++;
        if (str[i] < '0' || str[i] > '9')
            return 0;
        i++;
    }
    if (dotCount > 1)
        return 0;
    return 1;
}

int stringValidation(char *str)
{
    if (str[0] == '\'' && str[strlen(str) - 1] == '\'')
        return 1;
    return 0;
}

void insert(char client_msg[STRING_LENGTH], int uid)
{
    if (strlen(globalPath) <= 0 && strlen(userPermission) <= 0)
        status_handler(NO_DATABASE_SELECTED);
    else
    {
        char cmd[STRING_LENGTH];
        strcpy(cmd, client_msg);
        char *token = strtok(cmd, " ");
        if (token && !strcmp(token, "INSERT"))
        {
            token = strtok(NULL, " ");
            if (token && !strcmp(token, "INTO"))
            {
                token = strtok(NULL, " ");
                if (token)
                {
                    char temp_tb[STRING_LENGTH];
                    strcpy(temp_tb, token);
                    char temp_tb_path[PATH_LENGTH * 2];
                    sprintf(temp_tb_path, "%s/%s.txt", globalPath, temp_tb);
                    FILE *tb_exist = fopen(temp_tb_path, "r");
                    if (!tb_exist)
                    {
                        status_handler(DATABASE_OR_TABLE_NOT_EXIST);
                        return;
                    }
                    else
                    {
                        char temp_total_datatypes[STRING_LENGTH];
                        char datatypes_check[STRING_LENGTH];
                        bzero(temp_total_datatypes, STRING_LENGTH);
                        bzero(datatypes_check, STRING_LENGTH);

                        fgets(temp_total_datatypes, STRING_LENGTH, tb_exist);
                        bzero(temp_total_datatypes, STRING_LENGTH);
                        fgets(temp_total_datatypes, STRING_LENGTH, tb_exist);
                        strcpy(datatypes_check, temp_total_datatypes);
                        fclose(tb_exist);
                        // printf("%s\n", temp_total_datatypes);

                        int count_value = 0;
                        int count_datatypes = 0;

                        token = strtok(NULL, " (");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                                count_value++;
                            token = strtok(NULL, " (");
                        }

                        token = strtok(temp_total_datatypes, "\t");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                                count_datatypes++;
                            token = strtok(NULL, "\t");
                        }

                        if (count_value != count_datatypes)
                        {
                            status_handler(INPUT_VALUE_NOT_MATCH_COLUMN_TOTAL);
                            return;
                        }

                        char temp_datatypes[count_datatypes][STRING_LENGTH];
                        token = strtok(datatypes_check, "\t");
                        count_datatypes = 0;
                        while (token)
                        {
                            // printf("%s\n", token);
                            if (strcmp(token, "\n"))
                            {
                                bzero(temp_datatypes[count_datatypes], strlen(temp_datatypes[count_datatypes]));
                                strcpy(temp_datatypes[count_datatypes], token);
                                while (temp_datatypes[count_datatypes][strlen(temp_datatypes[count_datatypes]) - 1] == '\n' ||
                                       temp_datatypes[count_datatypes][strlen(temp_datatypes[count_datatypes]) - 1] == '\t' ||
                                       temp_datatypes[count_datatypes][strlen(temp_datatypes[count_datatypes]) - 1] == ')')
                                    temp_datatypes[count_datatypes][strlen(temp_datatypes[count_datatypes]) - 1] = '\0';
                                // printf("DATA TYPE :%d  VALUE DATA TYPE: %s\n", count_datatypes, temp_datatypes[count_datatypes]);
                                count_datatypes++;
                            }
                            token = strtok(NULL, "\t");
                        }

                        char temp_value[count_value][STRING_LENGTH];
                        count_value = 0;
                        strcpy(cmd, client_msg);
                        char *token = strtok(cmd, " ");
                        token = strtok(NULL, " ");
                        token = strtok(NULL, " ");
                        token = strtok(NULL, " (");
                        while (token)
                        {
                            if (strcmp(token, "\n"))
                            {
                                bzero(temp_value[count_value], strlen(temp_value[count_value]));
                                strcpy(temp_value[count_value], token);
                                while (temp_value[count_value][strlen(temp_value[count_value]) - 1] == '\n' ||
                                       temp_value[count_value][strlen(temp_value[count_value]) - 1] == '\t' ||
                                       temp_value[count_value][strlen(temp_value[count_value]) - 1] == ' ' ||
                                       temp_value[count_value][strlen(temp_value[count_value]) - 1] == ',' ||
                                       temp_value[count_value][strlen(temp_value[count_value]) - 1] == ')' ||
                                       temp_value[count_value][strlen(temp_value[count_value]) - 1] == ';')
                                    temp_value[count_value][strlen(temp_value[count_value]) - 1] = '\0';
                                count_value++;
                                // printf("VALUE: %d VALUE: %s\n", count_value, temp_value[count_value - 1]);
                            }
                            token = strtok(NULL, " ");
                        }

                        // data type check with value
                        char insert_value[STRING_LENGTH];
                        bzero(insert_value, STRING_LENGTH);
                        for (int i = 0; i < count_datatypes; i++)
                        {
                            // printf("%s %s\n", temp_datatypes[i], temp_value[i]);
                            if (!strcmp(temp_datatypes[i], "int"))
                            {
                                if (!intValidation(temp_value[i]))
                                {
                                    status_handler(DATATYPE_NOT_MATCH);
                                    return;
                                }
                                else
                                {
                                    strcat(insert_value, temp_value[i]);
                                    strcat(insert_value, "\t");
                                }
                            }
                            else if (!strcmp(temp_datatypes[i], "float"))
                            {
                                if (!floatValidation(temp_value[i]))
                                {
                                    status_handler(DATATYPE_NOT_MATCH);
                                    return;
                                }
                                else
                                {
                                    strcat(insert_value, temp_value[i]);
                                    strcat(insert_value, "\t");
                                }
                            }
                            else if (!strcmp(temp_datatypes[i], "string"))
                            {
                                if (!stringValidation(temp_value[i]))
                                {
                                    status_handler(DATATYPE_NOT_MATCH);
                                    return;
                                }
                                else
                                {
                                    strcat(insert_value, temp_value[i]);
                                    strcat(insert_value, "\t");
                                }
                            }
                            // printf("insert Val%d %s\n", i, insert_value);
                        }
                        // printf("%s\n", insert_value);

                        FILE *tb_exist = fopen(temp_tb_path, "a+");
                        fprintf(tb_exist, "%s\n", insert_value);
                        fclose(tb_exist);

                        status_handler(SUCCESS_INSERT);
                    }
                }
                else
                    status_handler(WRONG_COMMAND);
            }
            else
                status_handler(WRONG_COMMAND);
        }
        else
            status_handler(WRONG_COMMAND);
    }
}

void userLog()
{
    time_t now;
    struct tm *new_time;

    time(&now);
    new_time = localtime(&now);
    char timestamp[STRING_LENGTH];
    bzero(timestamp, STRING_LENGTH);
    strftime(timestamp, sizeof(timestamp), "%F %T", new_time);

    FILE *log_file = fopen("usersLog.txt", "a");

    char temp_username[STRING_LENGTH];
    bzero(temp_username, STRING_LENGTH);
    strcpy(temp_username, username_active);
    char *token = strtok(temp_username, "\t");
    fprintf(log_file, "%s:%s:%s\n", timestamp, token, tempCommand);
    fclose(log_file);
}

int main()
{
    // CREATE DAEMON
    // pid_t pid;
    // pid = fork();
    // if (pid < 0)
    //     exit(EXIT_FAILURE);
    // if (pid > 0)
    //     exit(EXIT_SUCCESS);

    // SET DAEMON UP
    // umask(0);
    // pid_t sid = setsid();
    // if (sid < 0)
    //     exit(EXIT_FAILURE);
    // if ((chdir("/")) < 0)
    //     exit(EXIT_FAILURE);
    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);

    // CREATE SOCKET
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    check(server_socket, ERROR_SOCKET_CREATE);

    // SET SOCKET UP
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(LOCALPORT);
    server_address.sin_addr.s_addr = INADDR_ANY;

    // BIND
    int bind_status = bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address));
    check(bind_status, ERROR_SOCKET_BIND);

    // LISTEN
    int listen_status = listen(server_socket, 5);
    check(listen_status, ERROR_SOCKET_LISTEN);

    int client_socket;
    int addr_size = sizeof(server_address);
    char server_msg[STRING_LENGTH];
    char client_msg[STRING_LENGTH];
    int uid;

    // DAEMON LOOP
    while (1)
    {
        // DAEMON PROGRAM
        client_socket = accept(server_socket, (struct sockaddr *)&server_address, (socklen_t *)&addr_size);
        if (client_socket >= 0)
        {
            // GET USER TYPE
            recv(client_socket, client_msg, STRING_LENGTH, 0);
            if (!strcmp(client_msg, "ROOT"))
            {
                uid = ROOT_UID;
                bzero(server_msg, STRING_LENGTH);
                strcpy(server_msg, "SERVER SET TO ROOT MODE\n");
            }
            else
            {
                uid = USER_UID;
                bzero(server_msg, STRING_LENGTH);
                bzero(username_active, STRING_LENGTH);
                strcpy(username_active, client_msg);
                char user_check[STRING_LENGTH];
                int flag = 0;
                fclose(fopen("users.txt", "a"));
                FILE *users_list = fopen("users.txt", "r");
                while (fgets(user_check, STRING_LENGTH, users_list) != NULL)
                {
                    if (!strcmp(user_check, username_active))
                    {
                        bzero(server_msg, STRING_LENGTH);
                        strcpy(server_msg, "SERVER SET TO USER MODE\n");
                        flag = 1;
                        break;
                    }
                }
                if (!flag)
                {
                    bzero(server_msg, STRING_LENGTH);
                    strcpy(server_msg, "USER NOT FOUND");
                }
                fclose(users_list);
            }
            send(client_socket, server_msg, strlen(server_msg), 0);
            if (!strcmp(server_msg, "USER NOT FOUND"))
                continue;

            while (1)
            {
                bzero(client_msg, STRING_LENGTH);
                recv(client_socket, client_msg, STRING_LENGTH, 0);
                if (!strcmp(client_msg, EXIT_COMMAND))
                    break;

                char cmd[STRING_LENGTH];
                strcpy(cmd, client_msg);
                bzero(tempCommand, STRING_LENGTH);
                strcpy(tempCommand, client_msg);
                // function create
                if (!strcmp(strtok(cmd, " "), "CREATE"))
                    create(client_msg, uid);

                else if (!strcmp(strtok(cmd, " "), "USE"))
                    use(client_msg, uid);

                else if (!strcmp(strtok(cmd, " "), "DROP"))
                    drop(client_msg, uid);

                else if (!strcmp(strtok(cmd, " "), "GRANT"))
                    grant(client_msg, uid);
                else if (!strcmp(strtok(cmd, " "), "INSERT"))
                    insert(client_msg, uid);

                send(client_socket, status_msg, strlen(status_msg), 0);
                bzero(status_msg, STRING_LENGTH);
                if (strlen(tempCommand) > 0)
                    userLog();
            }
        }
    }
}